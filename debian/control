Source: wifiphisher
Section: net
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Build-Depends: debhelper-compat (=12),
               dh-python,
               dnsmasq-base,
               libnl-3-dev,
               libnl-genl-3-dev,
               libssl-dev,
               python3-all,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme
Standards-Version: 4.5.0
Homepage: https://github.com/sophron/wifiphisher
Vcs-Git: https://gitlab.com/kalilinux/packages/wifiphisher.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/wifiphisher

Package: wifiphisher
Architecture: all
Depends: dnsmasq-base,
         hostapd,
         iptables,
         net-tools,
         python3-pyric (>= 0.1.6+git20191210),
         ${misc:Depends},
         ${python3:Depends}
Description: Automated phishing attacks against Wi-Fi networks
 This package contains a security tool that mounts automated phishing attacks
 against WiFi networks in order to obtain secret passphrases or other
 credentials. It is a social engineering attack that unlike other methods it
 does not include any brute forcing. It is an easy way for obtaining credentials
 from captive portals and third party login pages or WPA/WPA2 secret
 passphrases.
